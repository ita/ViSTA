# ViSTA project change log
This file documents notable changes to ViSTA since version 1.14.


## ViSTA 1.16 - 2016-05-20

### General notes
ViSTA 1.16 now requires the msvc12 (Microsoft Visual Studio 2013) or gcc 4 compiler (version 4.8.2 or higher) to build.
For gcc, C++11 features are enabled (compiler flag "-std=c++11"), which means that this flag also has to be enabled in projects including many ViSTA header files.

### Removed
* As we're not supporting SVN anymore, the svn revision (cmake svn_rev) is not part of the ViSTA versioning anymore
* For the same reason, the SVN $Id$ lines (that could contain the last SVN commit if used correctly) have been removed
* The deprecated (and no longer implemented) methods VistaQuaternion::GetAnglesXYZ(), GetAnglesZXY() and GetAnglesZYX() have been removed
* The outdated Template project was removed from VistaDemo
* The obsolete StereoTester was removed from contrib
 
### Added
* The VistaOGLExt library is now part of the VistaCoreLibs (formerly part of VistaFlowLib). 
  VistaOGLExt provides several helpful classes and functions for when dealing with OpenGL and rendering stuff yourself, among them
  - VistaTexture: abstraction of an OpenGL texture
  - VistaGLSLShader: abstraction of a GLSL shader program. Note that this is for rendering own stuff, and cannot be used with OpenSG geometry.
  - VistaShaderRegistry: central class for easily loading and retrieving shader sources from a list of shader directories. 
  - VistaOGLUtils: collection of helper functions, e.g., for error checking
  - VistaGLLine and VistaGLPoints: provide simple ways to draw geometric lines and points using OpenGL line/point primitives.
  - VistaFrameBufferObj and VistaRenderBuffer: abstractions of framebuffer objects and render buffers
  - VistaABuffer*: realizes order-independent transparency
  - core classes for realizing particle and volume rendering
  - ... and some more

* VistaSimpleCallback can be used for callbacks without writing classes deriving from IVistaExplicitCallbackInterface, e.g., by just using lambda-functions
  Lambda functions can also be passed directly to VistaKeyboardSystemControl to register simple key callbacks

* The VistaGenericCreator can deal with non-default-constructible types
* Can now assign priorities to IntenSelect adapters (override IVistaIntentionSelectAdapter::GetContributionMultiplier())
* Unit tests for GenericFactory, SensorMeasure, HistoryProjectNode
* VistaCoreLibs and VistaDemos can now be built together using the CMakeLists.txt file in the top-level directory

### Changed
* The startscripts (in contrib) were improved to allow easier customization to the application more default options
* There is no invalid "default" zero-th sensor measure anymore if no data have been written, instead can be checked for validity

### Fixed
* Glut mouse wheel now works everywhere as expected
* All of the VistaDemos were fixed and should now work as expected. Documentation was also improved.
* A potential cause of memleaks was fixed in VistaSceneGraph::NewOpenGLNode() and VistaSceneGraph::NewExtensionNode
* The DTrack driver is now again usable without control connection (AttachOnly = data connection only)
* Fixed faulty registration of mouse / keyboard to multiple windows
* VistaStreamManager ignored one parameter and misinterpreted others upon log file creation
* Consistent usage of ReadState throughout drivers
* Correct projection behavior if the projection plane is set to exactly -1, 1, -1, 1
* Several warnings regarding implicit casts and comparisons
* Several small fixes/tweaks



## ViSTA 1.15 CONCORDE - 2016-01-21


### Deprecated:

* VistaCluster\[Master|Slave\] (old cluster mode)


### Removed:

* VistaTickTimer:
  - constructor had two optional parameters that were not used at all
  - static method RegisterEvents() (unused)

* VistaViewport:
  - VistaViewportProperties::MSG\_PARENT\_WINDOW\_SIZE\_CHANGE -
    window resizing may now directly call
  - VistaViewportProperties::SetSize/Position

  
### Added:

* VistaSystem:
  - GetStartUpTime()
  - command line parameter -disable\_stream\_colors to disable color
    streams when parsing the streams defined in the inifile.

* VistaOpenSGSystemClassFactory:
  - parameter to determine usage of OpenSG threads. Use of opensg
    thread factory can be configured in vista.ini SYSTEM section by
    setting USE\_OPENSG\_THREADS (default: true)

* VistaClusterMaster:
  - runtime deprecation warning when using old cluster mode

* VistaNewClusterMaster: 
  - configuration to record a session to file dumps
  - Init() overload for record-only
  - commandline option -record path to enable recording w/o editing
    configs (required NewClusterMaster or Standalone mode)
  - activate by adding ini entry RECORD = foldername to master
    section.
  - record folder from ini only set if no folder already present -->
    command line args have priority
  - recording replaces %DATE% in record filename with current date
  - max consecutive barrier failures configurable by ini entry
    MAX\_ALLOWED\_BARRIER\_FAILURES

* Vista\[New\]ClusterSlave:
  - profiler measuring glFinish call on slaves

* VistaClusterMode:
  - debugging output - can be enabled with DEBUG\_OUTPUT = TRUE in
    system config section of cluster-ini

* VistaZeroMQClusterBarrier:
  - WaitForConnection analogous to ZeroMQDataSync

* VistaRecordClusterLeaderDataSync, VistaReplayClusterFollowerSync:
  - DataSync that write to/read from file

* VistaReplaySlave:
  - replay a previously recorded session - activate by command line
    switch -replay foldername

* VistaDisplayBridge:
  - GetViewerPosition/Orientation with return value

* VistaWindow: 
  - VistaWindowPorperties::G/SetIsOffscreenBuffer for rendering into
    FBO (ini entry: OFFSCREEN_BUFFER)
  - G/SetDrawBorder (ini entry: DRAW_BORDER) re-added and
    implemented
  - G/SetContextVersion (ini entry: CONTEXT\_VERSION)
  - G/SetIsDebugContext (ini entry: DEBUG\_CONTEXT)
  - G/SetIsForwardCompatible (ini entry: FORWARD\_COMPATIBLE)
  - G/SetMultiSamples - can be used to explicitely request MSAA - plus
    implementation in GlutWindowToolkit (OSG not done yet).  This also
    allows MSAA to be used with offscreen buffer windows. Can be
    specified with the MULTISAMPLES entry in the WINDOWs ini
    section. 0 or less as "use system default", 1 as "disable MSAA",
    higher as nr of samples.
  - Get\[RBG|Depth\]Image to retrieve images from frame buffer
  - image read implementation for osg windows

* VistaViewport:
  - added VistaViewportProperties fields ResizeWithWindow and
    RepositionWithWindow, enabled by default, which control if the
    viewport pos and size should be maintained in relation to the
    window size
  
* VistaViewportResizeToProjectionAdapter: can be used to automatically
  update projections after resizing a viewport

* VistaFrameSeriesCapture: 
  - automatically creates a series of screenshots of a session
  - VistaSystem command line options -capture\_frames
    -capture\_frames\_periodically -capture\_frames\_with\_framerate
    to automatically set up FrameSeriesCapture
  - option -capture\_frames\_filename to specify filenames for
    capture\_frames\_* mode
  - allows tags (e.g. %S%, %D%) to be enclosed by $ instead of %

* VistaAdaptipeVSyncControl: 
  - monitor framerate and adaptively enable/disable vsync

* VistaSceneGraph/VistaOpenSGNodeBridge:
  - API to query all light nodes
  - non-static GetAllSubNodesOfType without FromNode, using root

* VistaGeometry: 
  - Clone() method
  - GetCoordinate with VistaVector3D return type
  - GetTrianglesTextureCoordinateIndices
  - completed Get* API for texcoords and colors (analogous to normals
    and coords), also affects Vista\[OpenSG\]GraphicsBridge

* VistaGeometryFactory::CreatePlane/CreateBox:
  - now have three types to generate faces: quads, quads split into
    two tris, and quads split symmetrically into four triangles
  - optionally accept scaled texture coordinates
  - static Create*Data functions which generate only the data vectors
    needed for the different primitive types without depending on the
    VistaSceneGraph member variable

* VistaLightNode: 
  - getter/setter using VistaColor

* VistaOpenGLPrimitiveList: 
  - per primitive coloring possible

* VistaKeyboardSystemControl:
  - key callback to create a screenshot (S)
  - key callback to reset VistaKernel profiling (ALT+i)
  - key callback for joint Profiler output from all cluster nodes
    (ALT+o)
  - keyboard key aliases for modifiers (ctrl, alt, del).
  - keyboard driver detecting ctrl, alt, del presses/releases with
    freeglut 2.8
  - GetKeyValueFromString now also knows shift/alt/ctrl key values
  - more named variants for string-to-keycode in GetKeyValueFromString
  - added modifier keys to GetKeyName
  - now optionally delays calls to registered actions

* VistaIntentionSelect:
  - ClearAllNodes() method

* IVista3DText:
  - G/SetIsLit to choose wether or not to use shading

* VistaFramerateDisplay:
  - new options. per default shown on all viewports, can be changed
    with ALL\_VIEWPORTS\[bool\], VIEWPORTS\[list<string>\] and
    EXCLUDE\_VIEWPORTS\[list<string>\]

* VistaColorOverlay:
  - Scene Overlay to draw simple unicolor fullscreen quad, e.g. for
    fading

* VistaOpenSGMultiMaterial:
  - Clone() method


### Changed:

* Build system:
  - request ZeroMQ version with PGM support
  - glew for offscreen buffer rendering
  - using SYSTEM_HEADERS define for OpenSG to suppress gcc compiler
    warnings in their header files.

* VistaSystem: 
  - unregister signal-handlers on deletion
  - Linux: register signal handlers with SA_RESTART so all restartable
    syscalls will be restarted automatically
  - VistaSystemConfigurators uses VistaKernelWindowHandle (VDD)
  - capturing default history size now in DriverInfo (cluster-device init)
  - ini configuration of BACKGROUNDCOLOR and LIGHTS now interprets
    colors as numbers or as text (e.g. BLACK, VISTA_BLUE)
  - Loading driver plugins worked even if driver type was wrongly
    capitalized in ini, which led to follow-up problems (i.e. a crash
    on exit). Driver types must now always be provided with correct
    case.

* VistaEventManager: 
  - UnregisterObserver outside frame not queued

* VistaEventHandler: 
  - S/GetIsEnabled now virtual

* VistaEvent:
  - clarified API (using enum instead of bool on construction to
    clarify waitable / non-waitable behavior)

* VistaThreadEvent:
  - ResetThisEvent: added flag to indicate whether to reset ALL or
    just the latest event (depending on implementation)

* VistaTickTimer:
  - changed timer event type registration

* VistaDisplayManager:
  - MakeScreenshot function changed - now no longer uses OpenSG
    Foreground, but directly grabs framebuffer. This allows to (a)
    grab the whole window, instead of just the first viewport, and (b)
    grab images from multisample FBOs
    
* VistaDisplayBridge:
  - G/SetFullscreen renamed to G/SetWindowFullscreen

* VistaOpenSGDisplayBridge:
  - added notification message to clarify on size change (parent
    window size change in display bridge), to differentiate from vp
    size change

* VistaWindowingToolkit:
  - support window aspect changes

* VistaGlutWindowingToolkit:
  - handler for catching window close
  - can now optionally catch all exceptions that are unhandled, before
    returning control to glut. This can be helpful with 64bit-windows,
    which silently smallows unhandled expressions. Can be enabled by
    setting the CMake vars
    VISTACORELIBS\_GLUT\_WINDOW\_NOTIFY\_FRAME\_EXCEPTIONS or
    VISTACORELIBS\_GLUT\_WINDOW\_ABORT\_ON\_FRAME\_EXCEPTIONS
  - by default leaves multisampling unchanged from system default
  - hide dummy window immediately after creation
 
* VistaGraphicsBridge/VistaOpenSGGraphicsBridge:
  - more const correctness
  - SetTextures configures textures to use repeat and mipmaps.
  - SetTransparency checks for SimpleMaterial special case.
  - GetCachedOrLoadImage: texture loading on windows now accepts
    absolute paths.

* VistaSceneGraph:
  - GetAllSubNodesOfType is now static
  - SaveSubTree now accepts IVistaNode instead of VistaNode

* VistaOpenGLNode: 
  - using top-level GetBoundingBox

* VistaGeometryFactory::CreateBox: 
  - create box with no superfluous vertices
  - un-set backface culling

* VistaInteractionManager:
  - now acts as read state source for all devices in the event Loop

* VistaKeyboardSystemControl:
  - by default, keyboard actions are now triggered in the
    POSTAPPLICATION event, because otherwise issues may arise when
    using cluster-synchronization-dependent actions (e.g. ClusterSync
    usage), and performing time-consuming operations have a higher
    impact because they are first performed on master, then on slave

* VistaDfnKeyCallbackNode: 
  - only update outport when actually changed
  - moved setup of keyboard callbacks into dedicated function

* VistaIntentionSelect:
  - VistaNodeAdapter::GetNode() returns the Vista node.
  - now also supports line handles
  - IVistaIntentionSelectAdapter::GetPosition now also gets the
    reference frame of the IntenSelect cone

* VistaAxes:
  - takes parent group node as second parameter now

* VistaOSGAC3DFileType: 
  - loader doesn't create per-vertex colors anymore (ac3d has
    per-object colors only)

* VistaFadeoutNode: overlay extracted to VistaColorOverlay

* VistaProximityWarning: no longer flashes when entering from the outside




### Fixed:

* Build system:
  - GLUT capitalization in vista\_find\_package call

* VistaSystem:
  - check if ini paths exist before trying to make absolute
  - no longer crashes when closed via console on MS Windows
  - use GetSystemSectionName() instead of hardcoded "SYSTEM"
  - adding a call to disconnect after disabling device drivers on
    ramp-down of system in d'tor
  - console Handlers for windows equiv. to behavior on posix signals
    (i.e. Ctrl+C -> quit, 3 times Ctrl+C -> Force quit)
  - Linux SIGTERM adjustments

* VistaSystemConfigurators:
  - remove USB connection
  - set driver/sensor names in SensorMappingConfigurator only if not
    yet set by the driver itself

* VistaClusterStandalone:
  - now provides correct node counts and infos

* VistaNewClusterMaster:
  - proper observing of swapsync barriers to detect dropped slaves
  - cluster debug stream config deserialization
  
* VistaZeroMQClusterDataSync:
  - make cluster sync work with >=zeromq-3.2

* VistaGlutWindowingToolkit:
  - SetUseStereo: proper init checking
  - problem with freeglut 2.8 calling closefunction after window was
    already destroyed, crashing on shutdown
  - Combining real and offscreen windows works properly now
  - after window resizing, the window still reported the old size
    until the next frame (i.e. until the glut reshape callback
  - Check for freeglut version (context version and flags only
    available since freeglut 2.8)
  - conditional use of GL\_MAX\_FRAMEBUFFER\_WIDTH/HEIGHT
  - VSync initial setting
  - GlutImage Read\[RGB|Depth\]Image read from front buffer instead of
    back buffer

* VistaOpenSGDisplayBridge:
  - GetViewportPosition returned wrong y position

* VistaViewport:
  - SetSize did set viewport width/height one pixel too large (wrong
    conversion to opensg's left/top pixel)

* VistaViewportResizeToProjectionAdapter:
  - div by zero when viewport is temporarily resized to zero width or
    height

* VistaOSGWindowingToolkit:
  - add empty(!) methods to make it compile, due to changes in
    VistaWindowingToolkit

* VistaUser/PlatformNode: 
  - initial state setting

* VistaOpenSGGraphicsBridge:
  - vertex count computation for lines
  - GetCoordinates crash
  - GetTrianglesNormalIndices: proper check for normal correctness
  - GetTrianglesTextureCoordinateIndices: index check

* VistaSceneGraph:
  - point lights defined using prop lists (e.g., from a config file)
    now work ... (direction was used instead)
  - when changing the parent of a light node, the light got disabled
    (only light beacon was reattached to scenegraph, but not actual
    light).

* VistaGeometry:
  - GetColor properly handles osg::SimpleMaterial

* VistaGeometryFactory:
  - usage of resolution value in triangle creation
  - ordering of vertices in triangle creation
  - CreateEllipsoidData default parameters and order

* VistaInteractionManager:
  - DelInteractionContext: proper deletion iteration

* VistaVirtualConsole:
  - Set correct texture unit (prevents problems when using
    OpenSSGShadows)

* VistaKeyboardSystemControl::GetKeyAndModifiersValueFromString():
  - parsing of plus-sign as character, not separator
  - corrected name check for Pagedown
  - BindAction with Force properly overwrites in all cases

* VistaFramerateDisplay: avoiding stack corruption in framerate
  display for ludicrous framerates.

* VistaEyeTester: 
  - proper GL tex env mode setting
  - eyetester texture always applied

* VistaDfnFadeoutNode: corrected fadeout opacity adjustment
   
* Various minor fixes and style changes
