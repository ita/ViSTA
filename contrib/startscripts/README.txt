These files allow running an application with the top-level run.sh script and allows addition of different setups.
In addition to the top-level run.sh, there are several scripts in the startscripts subfolder:
  start.sh					system-independant configuration and actual starting of the application
per SYSTEM:
  start_SYSTEM.sh			system-specific parameters for the application, should call start.sh
per SYSTEM that runs in cluster mode
  kill_SYSTEM.sh			generic script to kill all applications on the cluster of the respective system
  master_SYSTEM.sh			generic script to start the master on the cluster of the respective system
  slave_SYSTEM.sh			generic script to start one slave on the cluster of the respective system
  startslaves_SYSTEM.sh		generic script to start all slaves on the cluster of the respective system

In the simplest case, just change settings in startscripts/application_settings.sh

If you use differently named initialization files (other that vista_SYSTEM.ini and display_SYSTEM.ini), these have to be adjusted in the start_SYSTEM.sh files,
where you also can pass additional system-dependent parameters, change antialiasing settings, or load the correct compiler.
Additional parsing of parameters should be performed in either start.sh or run.sh, and documented in the print_additional_usage_output() call in application_settings.sh.