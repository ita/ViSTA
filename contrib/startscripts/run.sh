#!/bin/bash

# Script to run an application with a specific system configuration
# Modify this to select the available system setups and provide a system
# setup's default cluster mode iff it runs in cluster mode. For each
# system setup, one can also provide a list of hostnames in order to
# determine the default setup on those hosts if none is specified as parameter.
# Most application-dependent settings should be changed in startscripts/application_settings.sh

# list available system setups here
# Standalone configurations require a script start_SYSTEM.sh.
# Cluster configurations also require scripts master_SYSTEM.sh, slave_SYSTEM.sh, and startslaves_SYSTEM.sh.
# These scripts are to be located in a startscripts subfolder
SYSTEMSETUPS=( desktop aixcave imflip )

# cluster modes have to define a DEFAULT_CLUSTERCONFIG
# If no DEFAULT_CLUSTERCONFIG is defined for a configuration, it is considered to be standalone
aixcave_DEFAULT_CLUSTERCONFIG=HoloSpace

# specifies the number of seconds to wait between starting the slaves and the master
WAIT_AFTER_SLAVE_START=5

# specifies the subdirectory in which the other startscripts are located
STARTSCRIPTS_SUBDIR="startscripts"

# for each config, you may optionally specifiy a hostname (as regex)
# if this script is executed without specifying a system config, the
# corresponding config is started by default
desktop=( 'vrdev.*' 'linuxgpud.*' )
aixcave=( 'linuxgpum1.*' )

# choose a default of wether or not the slave output should be redirected
# to log files in the slavelogs subfolder. Can be overwritten wit -R / -NR
export REDIRECT_SLAVE_OUTPUT=true
export REDIRECT_SLAVE_OUTPUT_FOLDER="slavelogs"

# this is a list of additional parameters. in general, all parameters that
# are not parsed by the run.sh file are forwarded to the next scripts in
# the same order. To add additional parameters, add them to the following
# two vars. Parameters in ADDITIONAL_PARAMS_PRE will passed in front of any
# additional parameters to this scripts, those in ADDITIONAL_PARAMS_POST after
ADDITIONAL_PARAMS_PRE=
ADDITIONAL_PARAMS_POST=

KILL_AFTER_EXECUTION=true

########################################
# Don't edit below this line           #
# (unless you know what you're doing)  #
########################################


CURRENT_SCRIPT_DIRECTORY=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
STARTSCRIPTS_DIRECTORY="$CURRENT_SCRIPT_DIRECTORY/$STARTSCRIPTS_SUBDIR"

source "$STARTSCRIPTS_DIRECTORY/application_settings.sh"

# usage/help output for parameters parsed by this file
print_usage_output()
{
	echo "Usage: $0 [System] [RUN_OPTIONS] [START_PARAMS] [APP_PARAMS]"
	echo "    runs ${EXECUTABLE_NAME_RELEASE} as the desired system configuration"
	echo "         System:  specifies the System setup config of the app,"
	echo "                  can be ${SYSTEMSETUPS[@]}"
	echo "                  if no System is specified, this script tries to"
	echo "                  determine it from the hostname"
	echo "    RUN_OPTIONS:  advanced configuration of the run script"
	echo "                -C --cluster-config ClusterConfig"
	echo "                     Cluster Configuration (e.g. Holospace, Frontwall) to use instead"
	echo "                     of the default (ClusterMode only)"
	echo "                -X --exclude-slaves n1 n2 ..."
	echo "                     excludes the listed slave nodes"
	echo "                -R --redirect-slave-output"
	echo "                     redirect slave output to file in slavelogs subdir"
	echo "                -NR --dont-redirect-slave-output"
	echo "                     don't redirect slave output to file"
	echo "                -RF --redirect_folder"
	echo "                     files containing redirected slaves output will be stored in this folder"
	echo "                -M --master_only"
	echo "                     run only the slave instances in debug mode (ClusterMode only)"
	echo "                -S --slaves_only"
	echo "                     run only the slave instances in debug mode (ClusterMode only)"
	echo "                -K --kill"
	echo "                     don't start the application, only execute the kill script"
	echo "                -SC --slave-config-file file"
	echo "                     specify an override for the slave configuration file (may be used ba startslaves_SYSTEM.sh)"
	echo "                -W --wait time_in_seconds"
	echo "                     changes the wait duration between starting slaves and starting the master"
	echo "                -DK --dont_kill"
	echo "                     dont execute kill script after launching a cluster configuration"
	echo "    START_PARAMS:  Parameters parsed and interpreted by start.sh"
	print_additional_usage_output
}

CLUSTER_CONFIGURATION=
CHOSENSYSTEM=

START_MASTER=true
START_SLAVES=true

if [ "$1" == "-h" -o "$1" == "--help" ]; then
	print_usage_output
	exit
fi

# check if the first parameter describes a system configuration
INPUTSYSTEM_LOWERCASE=`echo $1  | tr '[A-Z]' '[a-z]'`
for TESTSYSTEM in ${SYSTEMSETUPS[@]}; do
	if [ "${INPUTSYSTEM_LOWERCASE}" == "$TESTSYSTEM" ];    then
		shift
		CHOSENSYSTEM=$TESTSYSTEM
		break
	fi
done

# if no system was found from the first parameter, check there
# is a default config for this hostname
if [ "$CHOSENSYSTEM" == "" ]; then
	for TESTSYSTEM in ${SYSTEMSETUPS[@]}; do
		HOST_PATTERN_LIST="$TESTSYSTEM[@]"
		for HOST_PATTERN in ${!HOST_PATTERN_LIST}; do
			if [[ $HOSTNAME =~ $HOST_PATTERN ]]; then
				CHOSENSYSTEM=$TESTSYSTEM
			fi
		done
	done
fi

if [ "$CHOSENSYSTEM" == "" ]; then
	echo "run.sh - no suitable system found"
	print_usage_output
	exit -1
fi

# parse parameters

while [ ! "$1" == "" ]; do

	if [ "$1" == "-R" -o "$1" == "--redirect-slave-output" ]; then
		export REDIRECT_SLAVE_OUTPUT=true
		shift
	elif [ "$1" == "-NR" -o "$1" == "--dont-redirect-slave-output" ]; then
		export REDIRECT_SLAVE_OUTPUT=false
		shift
	elif [ "$1" == "-RF" -o "$1" == "--redirect-folder" ]; then
		shift
		export REDIRECT_SLAVE_OUTPUT_FOLDER=$1
		shift
	elif [ "$1" == "-C" -o "$1" == "--cluster-config" ]; then
		CLUSTER_CONFIGURATION=$2
		shift
		shift
	elif [ "$1" == "-M" -o "$1" == "--master_only" ]; then
		START_MASTER=true
		START_SLAVES=false
		shift
	elif [ "$1" == "-S" -o "$1" == "--slaves_only" ]; then
		START_MASTER=false
		START_SLAVES=true
		shift
	elif [ "$1" == "-K" -o "$1" == "--kill" ]; then
		if [ -f "$STARTSCRIPTS_DIRECTORY/kill_$CHOSENSYSTEM.sh" ]; then
			echo "executing kill script"
			"$STARTSCRIPTS_DIRECTORY/kill_$CHOSENSYSTEM.sh"
			exit
		else
			killall -9 $EXECUTABLE_NAME_RELEASE
			killall -9 $EXECUTABLE_NAME_DEBUG
			exit
		fi
	elif [ "$1" == "-X" -o "$1" == "--exclude-slaves" ]; then
		shift
		while [[ "$1" =~ ^[0-9]+$ ]]; do
			if [ "$1" -gt 9 ]; then
				EXCLUDE_VAR_NAME=SKIP_SLAVE$1
			else
				EXCLUDE_VAR_NAME=SKIP_SLAVE0$1
			fi
			eval export $EXCLUDE_VAR_NAME=1
			shift
		done
	elif [ "$1" == "-SC" -o "$1" == "--slave-config" ]; then
		shift
		export SLAVENODES_CONFIGURATION_FILE=$1
		echo "overriding slave node configuration file to $SLAVENODES_CONFIGURATION_FILE"
		shift
	elif [ "$1" == "-W" -o "$1" == "--wait" ]; then
		shift
		WAIT_AFTER_SLAVE_START=$1
		echo "setting wait time between slaves and master to $WAIT_AFTER_SLAVE_START"
		shift
	elif [ "$1" == "-DK" -o "$1" == "--dont_kill" ]; then
		shift
		KILL_AFTER_EXECUTION=false
		shift
	else
		# unknown parameters --> end of run.sh params
		break
	fi
done

DEFAULT_CLUSTERCONFIG_VAR=${CHOSENSYSTEM}_DEFAULT_CLUSTERCONFIG
DEFAULT_CLUSTERCONFIG=${!DEFAULT_CLUSTERCONFIG_VAR}

if [ "${DEFAULT_CLUSTERCONFIG}" == "" ]; then
	# no DEFAULT_CLUSTERCONFIG exists, thus we run it in standalone mode

	if [ ! -f "$STARTSCRIPTS_DIRECTORY/start_$CHOSENSYSTEM.sh" ]; then
		echo "run.sh - Config \"$CHOSENSYSTEM\" selected for Standalone, but script start_$CHOSENSYSTEM.sh not found"
		exit -1
	fi

	"$STARTSCRIPTS_DIRECTORY/start_$CHOSENSYSTEM.sh" $ADDITIONAL_PARAMS_PRE $@ $ADDITIONAL_PARAMS_POST
else

	if [ ! -f "$STARTSCRIPTS_DIRECTORY/master_$CHOSENSYSTEM.sh" ] || [ ! -f "$STARTSCRIPTS_DIRECTORY/slave_$CHOSENSYSTEM.sh" ] || [ ! -f "$STARTSCRIPTS_DIRECTORY/startslaves_$CHOSENSYSTEM.sh" ]; then
		echo "run.sh - Config \"$CHOSENSYSTEM\" selected for ClusterMode, but not all required scripts (startslaves_$CHOSENSYSTEM.sh, master_$CHOSENSYSTEM.sh, slave_$CHOSENSYSTEM.sh) exist"
		exit -1
	fi

	if [ "$CLUSTER_CONFIGURATION" == "" ]; then
		CLUSTER_CONFIGURATION=$DEFAULT_CLUSTERCONFIG
	fi


	if $START_SLAVES; then
		echo ""
		echo "starting slaves"
		"$STARTSCRIPTS_DIRECTORY/startslaves_${CHOSENSYSTEM}.sh" $CLUSTER_CONFIGURATION $ADDITIONAL_PARAMS_PRE $@ $ADDITIONAL_PARAMS_POST
		
		sleep $WAIT_AFTER_SLAVE_START
		echo ""
	fi
		
	if $START_MASTER; then
		echo "starting master"
		"$STARTSCRIPTS_DIRECTORY/master_$CHOSENSYSTEM.sh" $CLUSTER_CONFIGURATION $ADDITIONAL_PARAMS_PRE $@ $ADDITIONAL_PARAMS_POST
	fi
	
	sleep 1

	echo ""
	if $START_MASTER; then
		if [ "$KILL_AFTER_EXECUTION" == "true" ]; then
			if [ -f "$STARTSCRIPTS_DIRECTORY/kill_$CHOSENSYSTEM.sh" ]; then
				echo "executing kill script"
				"$STARTSCRIPTS_DIRECTORY/kill_$CHOSENSYSTEM.sh" >/dev/null 2>/dev/null
			fi
		fi
	fi

fi


