#!/bin/bash

# script to start the actual application without system-specific options
# Modify this to parse additional parameters, e.g. to start debug versions,
# and set any system-independent settings.
# System-dependent settings should be set in start_SYSTEM.sh.

CURRENT_SCRIPT_DIRECTORY=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "$CURRENT_SCRIPT_DIRECTORY/application_settings.sh"

EXECUTABLE_FOLDER="$CURRENT_SCRIPT_DIRECTORY/$EXECUTABLE_PATH"
EXECUTABLE_RELEASE="$EXECUTABLE_FOLDER/$EXECUTABLE_NAME_RELEASE"
EXECUTABLE_DEBUG="$EXECUTABLE_FOLDER/$EXECUTABLE_NAME_DEBUG"

EXECUTABLE="$EXECUTABLE_RELEASE"
ADDITIONAL_PARAMS=

# parsing arguments
while [ ! "$1" == "" ]; do

	if [ "$1" == "-D" -o "$1" == "--debug" ]; then
		shift
		EXECUTABLE="$EXECUTABLE_DEBUG"
	elif [ "$1" == "-DM" -o "$1" == "--debug_master" ]; then
		shift
		if [ "$IS_SLAVE" != "false" ]; then
			EXECUTABLE="$EXECUTABLE_DEBUG"
		fi
	elif [ "$1" == "-DS" -o "$1" == "--debug_slave" ]; then
		shift
		if [ "$IS_SLAVE" == "true" ]; then
			EXECUTABLE="$EXECUTABLE_DEBUG"
		fi
	elif [ "$1" == "-TV" -o "$1" == "--totalview" ]; then
		shift
		if [ "$IS_SLAVE" != "true" ]; then
			EXECUTABLE="totalview"
			ADDITIONAL_PARAMS="$EXECUTABLE_DEBUG -a"
		fi
	elif [ "$1" == "-TVS" -o "$1" == "--totalview_slave" ]; then
		shift
		if [ "$IS_SLAVE" == "true" ]; then
			EXECUTABLE="totalview"
			ADDITIONAL_PARAMS="$EXECUTABLE_DEBUG -a"
		fi
	elif [ "$1" == "-VG" -o "$1" == "--valgrind" ]; then
		shift
		if [ "$IS_SLAVE" != "true" ]; then
			EXECUTABLE="valgrind"
			ADDITIONAL_PARAMS="$EXECUTABLE_DEBUG"
		fi
	elif [ "$1" == "-VGS" -o "$1" == "--valgrind_slave" ]; then
		shift
		if [ "$IS_SLAVE" == "true" ]; then
			EXECUTABLE="valgrind"
			ADDITIONAL_PARAMS="$EXECUTABLE_DEBUG"
		fi	
	else
		# last recognized parameter, rest should be for the app
		break
	fi
done

cd "$CURRENT_SCRIPT_DIRECTORY/$WORKINGDIR_PATH"
source "$EXECUTABLE_FOLDER/$PATH_SCRIPT"
echo "running $EXECUTABLE $ADDITIONAL_PARAMS $@"
$EXECUTABLE $ADDITIONAL_PARAMS $@


