#!/bin/bash

# generic script to kill an application on the new CAVE at the RWTH Aachen University

########################################
# Generic, application-independet file #
# Don't edit!                          #
# (unless you know what you're doing)  #
########################################

CURRENT_SCRIPT_DIRECTORY=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $CURRENT_SCRIPT_DIRECTORY/application_settings.sh

killall -9 $EXECUTABLE_NAME_RELEASE &
killall -9 $EXECUTABLE_NAME_DEBUG &
/home/vrsw/gpucluster/bin/killcaveclients_nowait.sh $EXECUTABLE_NAME_RELEASE  &
/home/vrsw/gpucluster/bin/killcaveclients_nowait.sh $EXECUTABLE_NAME_DEBUG &
