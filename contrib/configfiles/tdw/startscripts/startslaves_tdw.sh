#!/bin/bash

# generic script to start the slaves of the tiled display wall at the RWTH Aachen University

########################################
# Generic, application-independet file #
# Don't edit!                          #
# (unless you know what you're doing)  #
########################################

CONFIGS=( FullWall SingleDisplay )
FullWall=( 1 2 3 4 5 6 )
SingleDisplay=( 2 )

SLAVENAMES=( BottomRight BottomMiddle BottomLeft TopRight TopMiddle TopLeft )

if [ "$1" = "" ];
then
	echo 'parameter missing - specify a cluster configuration ( ${CONFIGS[@]} )'
	exit
fi

INPUTCONFIG=${1,,}
shift
CHOSENCONFIG=""

for TESTCONFIG in ${CONFIGS[@]}
do	
	if [ "${TESTCONFIG,,}" == "$INPUTCONFIG" ];
	then
		CHOSENCONFIG=$TESTCONFIG
		break
	fi
done

if [ "$CHOSENCONFIG" == "" ];
then
	echo "Provided Config does not exist"
	echo "Configs are: ${CONFIGS[@]}"
	exit
else
	echo "Starting config $CHOSENCONFIG"
fi

SLAVELIST="$CHOSENCONFIG[@]"

DIR=`pwd`

if $REDIRECT_SLAVE_OUTPUT; then
	echo "Redirecting all slaves' output to ./slavelogs/slave_*.log"
	if [ ! -d "slavelogs" ]; then
		mkdir slavelogs
	fi
fi

for ID in ${!SLAVELIST}
do
	SLAVEHOSTNAME=linuxtdc$ID
	SLAVENAME=${SLAVENAMES[$ID-1]}

	if $REDIRECT_SLAVE_OUTPUT; then
		echo start on $SLAVEHOSTNAME as ${SLAVENAME}
		ssh $SLAVEHOSTNAME "cd ${DIR}; export XAUTHORITY=/var/run/Xauthority-vr;export DISPLAY=:0.0; ./startscripts/slave_tdw.sh ${SLAVENAME} $@ >slavelogs/slave_${SLAVENAME}.log 2>&1" &
	else

		echo start on $SLAVEHOSTNAME as ${SLAVENAME}
		ssh $SLAVEHOSTNAME "cd ${DIR}; export XAUTHORITY=/var/run/Xauthority-vr;export DISPLAY=:0.0; ./startscripts/slave_tdw.sh ${SLAVENAME} $@" &
	fi
done
