import os, sys, shutil, re

replacements = { ".+\.(cpp|h)$" : [ 
									( re.compile( r'^[ \t]*/\*[\s\*]*\$[ \t]*Id[^\n\r\$]*\$\s*\*/$', re.MULTILINE ), r'' ), # delete multi-line c-style comment containing $Id$ 
									( re.compile( r'[ \t]*//[ \t]*\$[ \t]*Id[^\n\r\$]*\$[ \t]*' ), r'' ), # delete single-line cpp-style comment containing only svn id tag
								],
				"(CMakeLists\.txt|.*\.cmake)$" : [
									( re.compile( r'[ \t]*#[ \t]*\$[ \t]*Id[^\n\r\$]*\$[ \t]*' ), r'' ), # delete Cmake comment containing only svn id tag
								],
				".+\.xml$" : [
									( re.compile( r'[ \t]*<!--[ \t]*\$[ \t]*Id[^\n\r\$]*\$[ \t]*-->[ \t]*' ), r'' ), # delete xml comment containing only svn id tag
								],
				".+\.ini$" : [
									( re.compile( r'[ \t]*#[ \t]*\$[ \t]*Id[^\n\r\$]*\$[ \t]*' ), r'' ), # delete ini file comment containing only svn id tag
								],
			}


def ReplaceContent( file, replacelist ):    
	#print( '    processing file ' + file )
	input = open( file, 'r' )
	filecontent = input.read()
	input.close()

	replaceCount = 0
	for replacerule in replacelist:
		replacement, rchanges = re.subn( replacerule[0], replacerule[1], filecontent )
		if rchanges > 0:
			filecontent = replacement
			replaceCount = replaceCount + rchanges

	if replaceCount > 0:
		print ( ' performed', replaceCount, 'replacements in ' + file )
		backupFile = file + '.BEFORE_SVN_ID_LINE_REMOVAL.BAK'
		if os.path.isfile( backupFile ) == False:
			print ( '        backing up original file to ' + backupFile )
			shutil.copy2( file, backupFile )
		outFile = open( file, 'r+' )
		outFile.read() 
		outFile.seek( 0 )
		outFile.write( filecontent )
		outFile.truncate()
		outFile.close()
	#else:
	#	print( '         no replacement' )

	  
def ProcessFiles( root, files ):
	for name in files:
		fullName = os.path.join( root, name )
		if os.path.isfile( fullName ):
			for pattern, replacement in replacements.items():
				if re.search( pattern, name ):
					ReplaceContent( fullName, replacement )
			
def UndoBackup( root, files ):
	for name in files:
		match = re.search( r'(.+)\.BEFORE_SVN_ID_LINE_REMOVAL\.BAK$', name )
		if match:
			fullName = os.path.join( root, name )
			fullRestoreName = os.path.join( root, match.group(1) )
			print( "reverting file " + fullRestoreName )
			os.remove( fullRestoreName )
			os.rename( fullName, fullRestoreName )


if len(sys.argv) > 1 and sys.argv[1] != "":
	if len(sys.argv) > 2 and sys.argv[2] == "-undo":
		startdir = sys.argv[1]	
		print( 'startdir: ' + startdir )
		walkres = os.walk( startdir )
		for root, dirs, files in os.walk( startdir ):
			UndoBackup( root, files )
	else:
		startdir = sys.argv[1]
		print( 'startdir: ' + startdir )
		walkres = os.walk( startdir )
		for root, dirs, files in os.walk( startdir ):
			ProcessFiles( root, files )
else:
	print( 'Usage:' )
	print( 'RemoveScnIdLines.py ConversionRootDir [-undo]' )
	print( '  ConversionRootDir specifies toplevel dir from which on all files and subfolders will be converted, automatically backing up changed files' )
	print( '  -undo - If provided, the backups from a prior conversion will be restored. NOTE: This removes all changes from th econversion, but also all amnual changes after the backup!' )